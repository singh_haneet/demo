/* gulpfile.js */
var
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    connect = require('gulp-connect'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat');

// source and distribution folder
var
    source = 'scss/',
    dest = 'assets/';

gulp.task('sass', function() {
  gulp.src('scss/main.scss')
  .pipe(sass({style: 'expanded'}))
  .pipe(gulp.dest('assets/css/'));
});

gulp.task('js', function() {
  gulp.src('js/*.js')
  .pipe(uglify())
  .pipe(concat('script.js'))
  .pipe(gulp.dest('assets/js/'))
  .pipe(connect.reload());
});

gulp.task('html', function() {
  gulp.src('**/*.html')
  .pipe(connect.reload());
});

gulp.task('watch', function() {
  gulp.watch('js/*.js', ['js']);
  gulp.watch('scss/**/*.scss', ['sass']);
  gulp.watch('**/*.html', ['html']);
});

gulp.task('connect', function() {
  connect.server({
    root: '.',
    livereload: true
  });
});

gulp.task('default', ['html', 'js', 'sass', 'watch', 'connect']);
